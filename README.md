<h1 align="center">Hi 👋, I'm Júlia Miranda</h1>
<h3 align="center">A passionate Computer Science student from Brasil</h3>


- 🔭 I’m currently working on **an RPA project.**

- 🌱 I’m currently learning **RPA, Javascript, Haskell and others.**

- 👯 I’m looking to collaborate on **anything.**

- 🤝 I’m looking for help with **RPA and automation.**

- 👨‍💻 Check me at [https://www.linkedin.com/in/julia-miranda-sousa/](https://www.linkedin.com/in/julia-miranda-sousa/)

- 💬 Ask me about **anything, I'm happy to help!**

- ⚡ Fun fact **I enjoy riding horses on my freetime.**

<h2 align="left">Languages and Tools:</h2>

<h3 align="left">University:</h3>

![C++](https://img.shields.io/badge/c++-%2300599C.svg?style=for-the-badge&logo=c%2B%2B&logoColor=white) 
![Java](https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=java&logoColor=white) 
![Qt](https://img.shields.io/badge/Qt-%23217346.svg?style=for-the-badge&logo=Qt&logoColor=white) <img src="https://upload.wikimedia.org/wikipedia/commons/1/1c/Haskell-Logo.svg" alt="haskell" width="40" height="40"/> </a> <a href="https://www.w3.org/html/" target="_blank">
  
  
<h3 align="left">Front-end Development:</h3>

  ![HTML5](https://img.shields.io/badge/html5-%23E34F26.svg?style=for-the-badge&logo=html5&logoColor=white) ![CSS3](https://img.shields.io/badge/css3-%231572B6.svg?style=for-the-badge&logo=css3&logoColor=white) ![JavaScript](https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E) 
  
![React](https://img.shields.io/badge/react-%2320232a.svg?style=for-the-badge&logo=react&logoColor=%2361DAFB) ![React Native](https://img.shields.io/badge/react_native-%2320232a.svg?style=for-the-badge&logo=react&logoColor=%2361DAFB) ![jQuery](https://img.shields.io/badge/jquery-%230769AD.svg?style=for-the-badge&logo=jquery&logoColor=white)
  
 
<h3 align="left">Prototypeing:</h3>
  
  ![Figma](https://img.shields.io/badge/figma-%23F24E1E.svg?style=for-the-badge&logo=figma&logoColor=white)
  
  
<h3 align="left">Back-end Development:</h3>
  
  ![NodeJS](https://img.shields.io/badge/node.js-6DA55F?style=for-the-badge&logo=node.js&logoColor=white) ![JavaScript](https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E)
  
<h3 align="left">Code Versioning:</h3>
  
  ![Git](https://img.shields.io/badge/git-%23F05033.svg?style=for-the-badge&logo=git&logoColor=white) ![GitHub](https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white) ![GitLab](https://img.shields.io/badge/gitlab-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white)
  
  
